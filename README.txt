QScience D2D Search Module

This is an extension of the D2D Search module
<http://drupal.org/project/d2dsearch> for the QScience distribution
<http://drupal.org/project/qscience_profile>.

Creates a form to look for content type papers in the network of
friends instances.

The papers found can be added to local database. If the ViJo module is
installed and enabled, papers can be used to create a virtual journal
based on keywords extracted from the title and abstract.
